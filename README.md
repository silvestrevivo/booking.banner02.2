# BOOKING BANNER 02.2 #

This is the repository for the banner proporsal for Booking.

### What I use here? ###

* HTML
* SASS/CSS
* InlinerCSS ( https://inliner.cm/ ) to make styles inline

### How are the branches organized? ###

* master => Version for production.
* native => Version developed with HTML & CSS (no JavaScript).
* greensock => Version developer with HTML, CSS and GreenSock for animations.

### How do I get set up? ###

* Make a new iframe tag in a html file.
* Set a width="160px" and height="600px" as attributes.
* In the src attribute you must set src="http://playground.uonsite.nl/booking/banner1/".

### Maybe future ###

* Use of TweenMax library of GreenSock ( https://greensock.com/tweenmax ) for the animations instead of pure CSS (animation). (https://www.youtube.com/watch?v=_42HpXlGpfk&index=1&list=PLkEZWD8wbltlSS_d_7tx_H_FBNVro8918)
* Code review
